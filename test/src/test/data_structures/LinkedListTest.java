package test.data_structures;

import model.data_structures.LinkedList;
import junit.framework.TestCase;

public class LinkedListTest extends TestCase{

	private LinkedList<Integer> lista;

	public void setupEscenario1(){

		lista = new LinkedList<>();
		for(int i=0; i<100; i++){
			lista.addFirst(i);
		}
	}

	public void testAddFirst1(){
		setupEscenario1();
		assertEquals(new Integer(99), lista.getFirst());
	}

	public void testAddFirst2(){
		setupEscenario1();
		for(int i=100; i<150; i++){
			lista.addFirst(i);
		}
		assertEquals(new Integer(149), lista.getFirst());
		assertEquals(150, lista.getSize());
	}

	public void testGetNodeN1(){
		setupEscenario1();
		try {
			assertEquals(new Integer(0), lista.get(100));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testGetNodeN2(){
		setupEscenario1();
		try {
			lista.get(101);
		} catch (Exception e) {	
			assertEquals("No existe un nodo en la posici�n dada", e.getMessage());
		}
	}

	public void testAddLast1(){
		setupEscenario1();
		for(int i=100; i<150; i++){
			lista.addLast(i);
		}
		try {
			assertEquals(new Integer(100), lista.get(101));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testAddLast2(){
		setupEscenario1();
		for(int i=100; i<150; i++){
			lista.addLast(i);
		}
		try {
			assertEquals(new Integer(149), lista.get(lista.getSize()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testAdd1(){
		setupEscenario1();
		Integer item = new Integer(130);
		int pos = 20;

		try {
			lista.add(item, pos);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			assertEquals(item, lista.get(pos));
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public void testAdd2(){
		setupEscenario1();
		Integer item = new Integer(130);
		int pos = lista.getSize() + 2;
		
		try {
			lista.add(item, pos);
		} catch (Exception e) {
			assertEquals("No existe la posici�n dada", e.getMessage());
		}		
	}


	public void testRemoveFirst(){
		setupEscenario1();
		for(int i=0; i<10; i++){
			try {
				lista.removeFirst();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		assertEquals(90, lista.getSize());
		assertEquals(new Integer(89), lista.getFirst());
	}

	public void testRemoveLast(){
		setupEscenario1();
		for(int i=0; i<10; i++){
			try {
				lista.removeLast();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		assertEquals(90, lista.getSize());
	}

	public void testRemove1(){
		setupEscenario1();
		try {
			lista.remove(5);
		} catch (Exception e) {
			e.getMessage();
		}
		assertEquals(99, lista.getSize());
	}
	
	public void testRemove2(){
		setupEscenario1();
		try {
			lista.remove(lista.getSize()+1);
		} catch (Exception e) {
			assertEquals("No se encontr� la posici�n dada", e.getMessage());
		}
	}
	
	public void testGetSize(){
		setupEscenario1();
		for(int i=100; i<150; i++){
			lista.addFirst(i);
		}
		assertEquals(150, lista.getSize());
	}
	
	public void testGetFirst(){
		setupEscenario1();
		for(int i=100; i<150; i++){
			lista.addFirst(i);
		}
		assertEquals(new Integer(149), lista.getFirst());
	}
	
}
