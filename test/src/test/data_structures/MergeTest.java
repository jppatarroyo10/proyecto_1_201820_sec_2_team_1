package test.data_structures;

import junit.framework.TestCase;
import model.data_structures.Merge;

public class MergeTest extends TestCase{
	
	private String[] a;
	
	public void setupEscenario1(){
		String palabra = new String("p,r,u,e,b,a,p,a,r,a,m,e,r,g,e");
		a = palabra.split(",");
	}
	
	public void testSort(){
		setupEscenario1();
		Merge.sort(a);
		assertTrue(Merge.isSorted(a));
	}
}
