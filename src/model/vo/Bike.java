package model.vo;

import java.util.Comparator;

public class Bike implements Comparable<Bike>{

    private int bikeId;
    private int totalTrips;
    private int totalDistance;
    private int totalDuration;

    public Bike(int bikeId, int totalTrips, int totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    @Override
    public int compareTo(Bike o) {
    	if(totalTrips < o.totalTrips) {return -1;}
    	else if(totalTrips > o.totalTrips) {return 1;}
    	else {return 0;}
    }
    

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public int getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
    
    public void setTotalTrips(){
    	totalTrips += 1;
    }
    public void setTotalDistance(int newDistance){
    	totalDistance += newDistance;
    }
    public void setTotalDuration(int newDuration){
    	totalDuration += newDuration;
    }
    
    class Comparacion3C implements Comparator<Bike>{

		@Override
		public int compare(Bike bici1, Bike bici2) {
			if(bici1.getTotalDuration() < bici2.getTotalDuration()) {return -1;}
			else if(bici1.getTotalDuration() > bici2.getTotalDuration()) {return 1;} 
			else {return 0;}
		}
    	
    }
    
}
