package model.data_structures;

import java.util.Iterator;

public interface ILista<T extends Comparable<T>> extends Iterable<T> {

	public void addFirst(T elem);

	public T remove(T elem);

	public int getSize();

	public T get(T elem);

	public T get(int pos) throws Exception;
	
	public boolean isEmpty();

    @Override
    public Iterator<T> iterator();
}
