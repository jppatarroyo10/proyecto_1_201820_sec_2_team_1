package model.data_structures;

import java.util.Iterator;

public class LinkedList<T extends Comparable<T>> implements ILista<T>{
	
	private Node<T> first;
	private Node<T> actual;
	private int size;

	public LinkedList()
	{
		first = null;
		actual = null;
	}

	public LinkedList(T item)
	{
		first = new Node<T>(item);
		actual = first;
		size = 1;
	}

	public void addFirst(T item)
	{
		Node<T> newFirst = new Node<T>(item);
		newFirst.setNext(first);
		first = newFirst;
		actual = first;
		size++;
	}

	public void addLast(T item)
	{
		Node<T> newLast = new Node<T>(item);
		if(first == null)
		{
			first = newLast;
		}
		else
		{
			Node<T> actual = first;
			while(actual.getNext()!= null)
			{
				actual = actual.getNext();
			}
			actual.setNext(newLast);
		}
		size++;
	}

	public void add(T item, int pos) throws Exception
	{
		Node<T> newNode = new Node<T>(item);
		int contador = 0;

		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		
		else if( pos > size + 1){
			throw new Exception("No existe la posici�n dada");
		}

		else
		{
			if(pos == 1){
				addFirst(item);
				
			}
			else if(pos == size + 1){
				addLast(item);
			
			}
			else{
				Node<T> actual = first;
				while(actual.getNext()!= null )
				{
					contador++;
					if(contador+1 == pos)
					{
						newNode.setNext(actual.getNext());
						actual.setNext(newNode);
						size++;		
					}
					actual=actual.getNext();
				}				
			}
		}		
	}

	public void removeFirst() throws Exception
	{
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			Node<T> actual = first;
			first = first.getNext();
			actual = first;
			actual.setNext(null);
			size--;
		}
	}

	public void removeLast() throws Exception
	{
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			Node<T> actual = first;
			while(actual.getNext().getNext() != null)
			{
				actual = actual.getNext();
			}
			actual.setNext(null);
			size--;
		}
	}

	public void remove(int pos) throws Exception
	{
		int contador = 0;
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			if(pos == 1){
				removeFirst();
			}
			else if(pos == size){
				removeLast();
			}
			else{
				Node<T> actual = first;
				while(actual.getNext() != null)
				{
					contador++;
					if(contador+1 == pos)
					{
						Node<T> nextNull = actual.getNext();
						actual.setNext(actual.getNext().getNext());
						nextNull.setNext(null);
						size--;
					}
					actual = actual.getNext();
				}
				throw new Exception("No se encontr� la posici�n dada");
			}
		}
	}

	public int getSize()
	{
		return size;
	}
	
	public T getFirst(){
		return first.getItem();
	}
	
	public T getNext(){
		return actual.getNext().getItem();
	}
	
	public T get(int pos) throws Exception{
		int contador = 0;
		if( first == null){
			throw new Exception("la lista est� vac�a");
		}
		else{
			Node<T> actual = first;
			while(actual != null){
				contador++;
				if(contador == pos){
					return actual.getItem();
				}
				actual = actual.getNext();
			}
			throw new Exception("No existe un nodo en la posici�n dada");
		}	
	}


	@Override
	public T remove(T elem) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T get(T elem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public Iterator<T> iterator() {
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>{
		private Node<T> current = first;
		
		public boolean hasNext(){
			return current != null;
		}
		public void remove(){}
		public T next(){
			T item = current.getItem();
			current = current.getNext();
			return item;
		}
	}
	

	

}
