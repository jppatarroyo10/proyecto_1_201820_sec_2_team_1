package model.data_structures;

import java.util.Iterator;


public class LinkedListStack<T> implements IPila<T>{

	private Node<T> top;
	private int size = 0;
	
	public LinkedListStack(){
		top = null;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>{
		private Node<T> current = top;
		
		public boolean hasNext(){
			return current != null;
		}
		public void remove(){}
		public T next(){
			T item = current.getItem();
			current = current.getNext();
			return item;
		}
	}
	
	@Override
	public boolean isEmpty() {
		return (size==0);
	}

	@Override
	public int getSize() {
		return size; 
	}

	@Override
	public void push(T t) {
		Node<T> newNode = new Node<T>(t);
		if(top == null){
			top = newNode;
		}
		else{
			newNode.setNext(top);
			top = newNode;
		}
		size++;
	}

	@Override
	public T pop(){
		if(top == null){
			return null;
		}
		T element = top.getItem();
		Node<T> nextTop = top.getNext();
		top.setNext(null);
		top = nextTop;
		size--;
		return element;		
	}
	
	public T getTop(){
		return top.getItem();
	}
	
}
