package model.data_structures;

import java.util.Comparator;

import Comparaciones.Comparacion3C;
import Comparaciones.Comparacion4A;
import Comparaciones.Comparacion4C;
import model.vo.Bike;
import model.vo.Trip;



public class Merge
{
	private static Comparable[] aux;
	
	private static void exch(Comparable[] a, int i, int j){
		Comparable t = a[i];
		a[i] = a[j];
		a[j] = t;
	}
	
	public static void sort(Comparable[] a){
		aux = new Comparable[a.length];
		sort(a, 0, a.length-1);
	}
	public static boolean isSorted(Comparable[] a){
		for(int i = 1; i<a.length; i++){
			if(less(a[i], a[i-1])) return false;
		}
		return true;
	}
	private static boolean less(Comparable v, Comparable w){
		return v.compareTo(w) < 0;
	}	
	private static void merge(Comparable[] a, int lo, int mid, int hi){
		int i = lo;
		int j = mid + 1;
		
		for(int k=lo; k<=hi; k++){
			aux[k] = a[k];
		}
		
		for(int k=lo; k<=hi; k++){
			if(i>mid){ a[k] = aux[j++];}
			else if(j>hi){ a[k] = aux[i++];}
			else if(less(aux[j], aux[i])){ a[k] = aux[j++];}
			else {a[k] = aux[i++];}
		}
	}

	private static void sort(Comparable[] a, int lo, int hi){
		if(hi<=lo) return;
		int mid = lo + (hi-lo)/2;
		sort(a, lo, mid);
		sort(a, mid+1, hi);
		merge(a, lo, mid, hi);
	}
	
//TODO -----------------------MERGE 2----------------------------------	
	
	public static void sort2(Comparable[] a){
		aux = new Comparable[a.length];
		sort2(a, 0, a.length-1);
	}
	private static boolean less2(Comparable<Bike> v, Comparable<Bike> w){
		Comparacion3C comp = new Comparacion3C();
		return comp.compare(v, w) < 0;
	}
	private static void merge2(Comparable[] a, int lo, int mid, int hi){
		int i = lo;
		int j = mid + 1;
		
		for(int k=lo; k<=hi; k++){
			aux[k] = a[k];
		}
		
		for(int k=lo; k<=hi; k++){
			if(i>mid){ a[k] = aux[j++];}
			else if(j>hi){ a[k] = aux[i++];}
			else if(less2(aux[j], aux[i])){ a[k] = aux[j++];}
			else {a[k] = aux[i++];}
		}
	}
	
	private static void sort2(Comparable[] a, int lo, int hi){
		if(hi<=lo) return;
		int mid = lo + (hi-lo)/2;
		sort2(a, lo, mid);
		sort2(a, mid+1, hi);
		merge2(a, lo, mid, hi);
	}
	
	
//TODO -----------------------MERGE 3----------------------------------
	public static void sort3(Comparable[] a){
		aux = new Comparable[a.length];
		sort3(a, 0, a.length-1);
	}
	
	private static boolean less3(Comparable<Trip> v, Comparable<Trip> w){
		Comparacion4A comp = new Comparacion4A();
		return comp.compare(v, w) < 0;
	}
	
	private static void merge3(Comparable[] a, int lo, int mid, int hi){
		int i = lo;
		int j = mid + 1;
		
		for(int k=lo; k<=hi; k++){
			aux[k] = a[k];
		}
		
		for(int k=lo; k<=hi; k++){
			if(i>mid){ a[k] = aux[j++];}
			else if(j>hi){ a[k] = aux[i++];}
			else if(less3(aux[j], aux[i])){ a[k] = aux[j++];}
			else {a[k] = aux[i++];}
		}
	}
	
	private static void sort3(Comparable[] a, int lo, int hi){
		if(hi<=lo) return;
		int mid = lo + (hi-lo)/2;
		sort3(a, lo, mid);
		sort3(a, mid+1, hi);
		merge3(a, lo, mid, hi);
	}
	
//TODO -----------------------MERGE 4----------------------------------
		public static void sort4(Comparable[] a){
			aux = new Comparable[a.length];
			sort4(a, 0, a.length-1);
		}
		
		private static boolean less4(Comparable<Trip> v, Comparable<Trip> w){
			Comparacion4C comp = new Comparacion4C();
			return comp.compare(v, w) < 0;
		}
		
		private static void merge4(Comparable[] a, int lo, int mid, int hi){
			int i = lo;
			int j = mid + 1;
			
			for(int k=lo; k<=hi; k++){
				aux[k] = a[k];
			}
			
			for(int k=lo; k<=hi; k++){
				if(i>mid){ a[k] = aux[j++];}
				else if(j>hi){ a[k] = aux[i++];}
				else if(less4(aux[j], aux[i])){ a[k] = aux[j++];}
				else {a[k] = aux[i++];}
			}
		}
		
		private static void sort4(Comparable[] a, int lo, int hi){
			if(hi<=lo) return;
			int mid = lo + (hi-lo)/2;
			sort4(a, lo, mid);
			sort4(a, mid+1, hi);
			merge4(a, lo, mid, hi);
		}
		
}
