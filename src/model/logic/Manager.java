package model.logic;

import api.IManager;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.LinkedList;
import model.data_structures.LinkedListQueue;
import model.data_structures.LinkedListStack;
import model.data_structures.Merge;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import com.opencsv.CSVReader;

public class Manager implements IManager {
	// Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1Q2/Divvy_Trips_2017_Q1.csv";

	// Ruta del archivo de trips 2017-Q2
	// TODO Actualizar
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q1Q2/Divvy_Trips_2017_Q2.csv";

	// Ruta del archivo de trips 2017-Q3
	// TODO Actualizar
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3Q4/Divvy_Trips_2017_Q3.csv";

	// Ruta del archivo de trips 2017-Q4
	// TODO Actualizar
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q3Q4/Divvy_Trips_2017_Q4.csv";

	// Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar
	public static final String TRIPS_Q1_Q4 = "";

	// Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Trips_2017_Q1Q2/Divvy_Stations_2017_Q1Q2.csv";

	// Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Trips_2017_Q3Q4/Divvy_Stations_2017_Q3Q4.csv";

	private LinkedList<Station> listStations = new LinkedList<Station>();
	private LinkedList<Trip> listTrips = new LinkedList<Trip>();
	private LinkedListQueue<Bike> queueBikes = new LinkedListQueue<>();

	public void loadStations(String stationsFile) {

		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(stationsFile));
			String[] nextLine;
			try {
				nextLine = reader.readNext();
				nextLine = reader.readNext();
				while (nextLine != null) {
					int id;
					String name;
					String city;
					double latitude;
					double longitude;
					int dpcapacity;
					LocalDateTime online_date;

					if (nextLine[0] != "") {
						id = Integer.parseInt(nextLine[0]);
					} else {
						id = 0;
					}

					name = nextLine[1];
					city = nextLine[2];

					if (nextLine[3] != "") {
						latitude = Double.parseDouble(nextLine[3]);
					} else {
						latitude = 0;
					}

					if (nextLine[4] != "") {
						longitude = Double.parseDouble(nextLine[4]);
					} else {
						longitude = 0;
					}

					if (nextLine[5] != "") {
						dpcapacity = Integer.parseInt(nextLine[5]);
					} else {
						dpcapacity = 0;
					}
					if(stationsFile == STATIONS_Q3_Q4){
						DateTimeFormatter format = DateTimeFormatter
								.ofPattern("M/d/yyyy H:mm");
						online_date = LocalDateTime.parse(nextLine[6], format);
						
						Station station = new Station(id, name, city, latitude,
								longitude, dpcapacity, online_date);
						listStations.addFirst(station);
					}
					else if(stationsFile == STATIONS_Q1_Q2){
						DateTimeFormatter format = DateTimeFormatter
								.ofPattern("M/d/yyyy HH:mm:ss");
						online_date = LocalDateTime.parse(nextLine[6], format);
						
						Station station = new Station(id, name, city, latitude,
								longitude, dpcapacity, online_date);
						listStations.addFirst(station);
					}
					
					nextLine = reader.readNext();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("no se encontr� el archivo");
			e1.printStackTrace();
		}
		System.out.println("Total estaciones cargadas en el sistema: "
				+ listStations.getSize());
	}

	public void loadTrips(String tripsFile) {

		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(tripsFile));
			String[] nextLine;
			try {
				nextLine = reader.readNext();
				nextLine = reader.readNext();
				while (nextLine != null) {
					int trip_id;
					LocalDateTime start_time;
					LocalDateTime end_time;
					int bikeId;
					int tripDuration;
					int from_station_id;
					String from_station_name;
					int to_station_id;
					String to_station_name;
					String userType;
					String gender;
					int birthyear;

					if (nextLine[0] != "") {
						trip_id = Integer.parseInt(nextLine[0]);
					} else {
						trip_id = 0;
					}

					DateTimeFormatter format = DateTimeFormatter
							.ofPattern("M/d/yyyy HH:mm:ss");
					start_time = LocalDateTime.parse(nextLine[1], format);
					end_time = LocalDateTime.parse(nextLine[2], format);

					if (nextLine[3] != "") {
						bikeId = Integer.parseInt(nextLine[3]);
					} else {
						bikeId = 0;
					}
					if (nextLine[4] != "") {
						tripDuration = Integer.parseInt(nextLine[3]);
					} else {
						tripDuration = 0;
					}
					if (nextLine[5] != "") {
						from_station_id = Integer.parseInt(nextLine[5]);
					} else {
						from_station_id = 0;
					}

					from_station_name = nextLine[6];

					if (nextLine[7] != "") {
						to_station_id = Integer.parseInt(nextLine[7]);
					} else {
						to_station_id = 0;
					}

					to_station_name = nextLine[8];
					userType = nextLine[9];

					if (userType == "Subscriber") {
						gender = nextLine[10];
						birthyear = Integer.parseInt(nextLine[11]);
					} else {
						gender = "";
						birthyear = 0;
					}

					Trip trip = new Trip(trip_id, start_time, end_time, bikeId,
							tripDuration, from_station_id, from_station_name,
							to_station_id, to_station_name, userType, gender,
							birthyear, "");
					listTrips.addFirst(trip);

					nextLine = reader.readNext();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("no se encontr� el archivo");
			e1.printStackTrace();
		}
		System.out.println("Total trips cargados en el sistema: "
				+ listTrips.getSize());
	}
	
	private int distanceTraveled(Trip trip){
		return 0;
	}
	
	public void loadBikes(){
		for(Trip actual : listTrips){
			int id = actual.getBikeId();
			boolean exist = false;
			for(Bike bici : queueBikes){
				if(bici.getBikeId() == id){
					exist = true;
					bici.setTotalTrips();
					bici.setTotalDistance(distanceTraveled(actual));
					bici.setTotalDuration(actual.getTripDuration());
					break;
				}
			}
			if(!exist){
				Bike bici = new Bike(actual.getBikeId(), 1, distanceTraveled(actual), actual.getTripDuration());
				queueBikes.enqueue(bici);
			}
		}
		System.out.println("----------------------------------------");
		System.out.println("N�mero de bicicletas: " + queueBikes.getSize());
		System.out.println("----------------------------------------");
	}

	public ICola<Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		
		LinkedListQueue<Trip> colaViajes = new LinkedListQueue<Trip>();
		
		for(Trip actual : listTrips){
			if(actual.getStartTime().compareTo(fechaInicial) >= 0 && actual.getStopTime().compareTo(fechaFinal) <= 0){
				colaViajes.enqueue(actual);
			}
		}
		Trip[] arregloTrips = new Trip[colaViajes.getSize()];
		for (int i = 0; i < arregloTrips.length; i++) {
			arregloTrips[i] = colaViajes.dequeue();
		}
		Merge.sort(arregloTrips);
		if (Merge.isSorted(arregloTrips)) {
			for (int i = 0; i < arregloTrips.length; i++) {
				colaViajes.enqueue(arregloTrips[i]);
			}
		}
	
		return colaViajes;
	}

	public ILista<Bike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {			
		LinkedListQueue<Bike> bicisEnFecha = new LinkedListQueue<>();
		LinkedList<Bike> BikeSortList = new LinkedList<>();
		for(Trip actual : listTrips){
			if(actual.getStartTime().compareTo(fechaInicial) >= 0 && actual.getStopTime().compareTo(fechaFinal) <= 0){
				int id = actual.getBikeId();
				boolean exist = false;
				for(Bike bici : bicisEnFecha){
					if(bici.getBikeId() == id){
						exist = true;
						bici.setTotalTrips();
						bici.setTotalDistance(distanceTraveled(actual));
						bici.setTotalDuration(actual.getTripDuration());
						break;
					}
				}
				if(!exist){
					Bike bici = new Bike(actual.getBikeId(), 1, distanceTraveled(actual), actual.getTripDuration());
					bicisEnFecha.enqueue(bici);
				}
			}
		}	
		Bike[] arreglo = new Bike[bicisEnFecha.getSize()];
		for(int i=0; i<arreglo.length; i++){
			arreglo[i] = bicisEnFecha.dequeue();
		}
		
		Merge.sort(arreglo);
		if(Merge.isSorted(arreglo)){
			for(int i=0; i<arreglo.length; i++){
				BikeSortList.addFirst(arreglo[i]);
			}
		}
		return BikeSortList;
	}

	public ILista<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		
		LinkedListStack<Trip> pilaAux = new LinkedListStack<Trip>();
		LinkedList<Trip> lista = new LinkedList<Trip>();
		
		for(Trip actual : listTrips){
			if(actual.getBikeId() == bikeId && actual.getStartTime().compareTo(fechaInicial) >= 0 && actual.getStopTime().compareTo(fechaFinal) <= 0){
				pilaAux.push(actual);
			}
		}
		
		Trip[] arreglo = new Trip[pilaAux.getSize()];
		for(int i=0; i<arreglo.length; i++){
			arreglo[i] = pilaAux.pop();
		}
		
		Merge.sort(arreglo);
		if(Merge.isSorted(arreglo)){
			for(int i=arreglo.length - 1; i>=0; i--){
				lista.addFirst(arreglo[i]);
			}
		}		
		return lista;		
	}

	public ILista<Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		LinkedList<Trip> lista = new LinkedList<>();	
		LinkedListStack<Trip> pila = new LinkedListStack<>();
		for(Trip actual : listTrips){
			if(actual.getEndStationId() == endStationId && actual.getStartTime().compareTo(fechaInicial) >= 0 && actual.getStopTime().compareTo(fechaFinal) <= 0){
				pila.push(actual);
			}
		}
		Trip[] arreglo = new Trip[pila.getSize()];
		for(int i=0; i<arreglo.length; i++){
			arreglo[i] = pila.pop();
		}
		Merge.sort3(arreglo);
		for(int i = arreglo.length-1; i>=0; i--){
			lista.addFirst(arreglo[i]);
		}	
		return lista;
	}

	public ICola<Station> B1EstacionesPorFechaInicioOperacion(
			LocalDateTime fechaComienzo) {
		return null;
	}

	public ILista<Bike> B2BicicletasOrdenadasPorDistancia(
			LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	public ILista<Trip> B3ViajesPorBicicletaDuracion(int bikeId,
			int tiempoMaximo, String genero) {
		return null;
	}

	public ILista<Trip> B4ViajesPorEstacionInicial(int startStationId,
			LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	public void C1cargar(String rutaTrips, String rutaStations) {
		loadTrips(rutaTrips);
		loadStations(rutaStations);
	}

	public ICola<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		
		LinkedListQueue<Trip> cola = new LinkedListQueue<>();
		ILista<Trip> lista = A3ViajesPorBicicletaPeriodoTiempo(bikeId, fechaInicial, fechaFinal);
		LinkedListStack<Trip> pilaValidacion = new LinkedListStack<>();
		
		for(Trip trip : lista){
			if(pilaValidacion.getSize() == 0){
				pilaValidacion.push(trip);
			}
			else{
				if(trip.getStartStationId() == pilaValidacion.getTop().getEndStationId()){
					pilaValidacion.push(trip);
				}
				else{
					cola.enqueue(trip);
					cola.enqueue(pilaValidacion.pop());	
					pilaValidacion.push(trip);
				}
			}
		}
		while(pilaValidacion.getSize() > 0){
			pilaValidacion.pop();
		}
		return cola;
	}

	public ILista<Bike> C3BicicletasMasUsadas(int topBicicletas) {
		loadBikes();
		LinkedList<Bike> listaBikes = new LinkedList<>();
		Bike[] arreglo = new Bike[queueBikes.getSize()];
		
		for(int i=0; i<arreglo.length; i++){
			arreglo[i] = queueBikes.dequeue();
		}
		
		Merge.sort2(arreglo);
		int i=0;
		while(i<topBicicletas){
			listaBikes.addLast(arreglo[i]);
			i++;
		}
		
		return listaBikes;
	}

	public ILista<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		ICola<Trip> cola = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
		LinkedList<Trip> lista = new LinkedList<>();
		LinkedListStack<Trip> pila = new LinkedListStack<>();
		
		for(Trip trip: cola){
			if(trip.getStartStationId() == StationId){
				trip.setIdentificador("Viaje de inicio");
				pila.push(trip);
			}
			else if(trip.getEndStationId() == StationId){
				trip.setIdentificador("Viaje de terminaci�n");
				pila.push(trip);
			}
		}
		Trip[] arreglo = new Trip[pila.getSize()];
		for(int i=0; i<arreglo.length; i++){
			arreglo[i] = pila.pop();
		}
		Merge.sort4(arreglo);
		for(int i=arreglo.length-1; i>=0; i--){
			lista.addFirst(arreglo[i]);
		}
		
		return lista;
		
	}
}
