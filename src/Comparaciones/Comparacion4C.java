package Comparaciones;

import java.util.Comparator;

import model.vo.Trip;

public class Comparacion4C implements Comparator<Comparable<Trip>> {

	// "Viaje de inicio" 
	// "Viaje de terminación"
	@Override
	public int compare(Comparable<Trip> trip1, Comparable<Trip> trip2) {
		int comparacion = 0;
		if(((Trip) trip1).getIdentificador() == "Viaje de inicio" && ((Trip) trip2).getIdentificador() == "Viaje de inicio" ){
			if(((Trip) trip1).getStartTime().compareTo(((Trip) trip2).getStartTime()) < 0) {comparacion = -1;}
			else if(((Trip) trip1).getStartTime().compareTo(((Trip) trip2).getStartTime()) > 0) {comparacion = 1;}
			else {comparacion = 0;}
		}
		else if(((Trip) trip1).getIdentificador() == "Viaje de inicio" && ((Trip) trip2).getIdentificador() == "Viaje de terminación" ){
			if(((Trip) trip1).getStartTime().compareTo(((Trip) trip2).getStopTime()) < 0) {comparacion = -1;}
			else if(((Trip) trip1).getStartTime().compareTo(((Trip) trip2).getStopTime()) > 0) {comparacion = 1;}
			else {comparacion = 0;}
		}
		else if(((Trip) trip1).getIdentificador() == "Viaje de terminación" && ((Trip) trip2).getIdentificador() == "Viaje de terminación" ){
			if(((Trip) trip1).getStopTime().compareTo(((Trip) trip2).getStopTime()) < 0) {comparacion = -1;}
			else if(((Trip) trip1).getStopTime().compareTo(((Trip) trip2).getStopTime()) > 0) {comparacion = 1;}
			else {comparacion = 0;}
		}
		else if(((Trip) trip1).getIdentificador() == "Viaje de terminación" && ((Trip) trip2).getIdentificador() == "Viaje de inicio" ){
			if(((Trip) trip1).getStopTime().compareTo(((Trip) trip2).getStartTime()) < 0) {comparacion = -1;}
			else if(((Trip) trip1).getStopTime().compareTo(((Trip) trip2).getStartTime()) > 0) {comparacion = 1;}
			else {comparacion = 0;}
		}	
		return comparacion;
	}

}
