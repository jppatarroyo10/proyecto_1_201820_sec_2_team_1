package Comparaciones;

import java.util.Comparator;

import model.vo.Trip;

public class Comparacion4A implements Comparator<Comparable<Trip>> {

	@Override
	public int compare(Comparable<Trip> trip1, Comparable<Trip> trip2) {
		if(((Trip) trip1).getStopTime().compareTo(((Trip) trip2).getStopTime()) < 0) {return -1;}
		else if (((Trip) trip1).getStopTime().compareTo(((Trip) trip2).getStopTime()) > 0) { return 1;}
		else {return 0;}
	}

}
