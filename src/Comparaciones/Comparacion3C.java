package Comparaciones;

import java.util.Comparator;

import model.vo.Bike;

public class Comparacion3C implements Comparator<Comparable<Bike>> {

	public int compare(Comparable<Bike> bici1, Comparable<Bike> bici2) {
		if(((Bike) bici1).getTotalDuration() < ((Bike) bici2).getTotalDuration()) {return 1;}
		else if(((Bike) bici1).getTotalDuration() > ((Bike) bici2).getTotalDuration()) {return -1;} 
		else {return 0;}
	}


}
